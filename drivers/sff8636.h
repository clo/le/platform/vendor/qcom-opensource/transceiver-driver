/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Copyright (c) 2022-2023 Qualcomm Innovation Center, Inc. All rights reserved.
 *
 * Code is derived from http://git.armlinux.org.uk/cgit/linux-arm.git/
 * tree/drivers/net/phy/sfp.h?h=cex7
 *
 */
#ifndef LINUX_SFF8636_H
#define LINUX_SFF8636_H

#define SFF8636_POWER_CLASS_LOW (BIT(1) | BIT(0))
#define SFF8636_POWER_CLASS_HIGH (BIT(2) | BIT(0))
#define SFF8636_POWER_CLASS_1TO4 (BIT(0))

#define SFF8636_POWER_CLASS_1 (0x00)
#define SFF8636_POWER_CLASS_2 (0x40)
#define SFF8636_POWER_CLASS_3 (0x80)
#define SFF8636_POWER_CLASS_4 (0xC0)
#define SFF8636_POWER_CLASS_5 (0x01)
#define SFF8636_POWER_CLASS_6 (0x02)
#define SFF8636_POWER_CLASS_7 (0x03)

/* Copper cable, unequalized */
#define SFF8636_TRANS_COPPER_UNEQUAL    (10 << 4)
#define VENDOR_CIG "CIG"
#define CIG_TX_DISABLE_WAIT (5)

#define SFF8636_PARAM_THRESHOLD_SET_MAX (16)
#define SFF8636_PARAM_CFG_SET_MAX (24)

enum {
    SFF8636_LOS                         = QSFP_ADDR(0, 0,   3),
    SFF8636_TX_FAULT                    = QSFP_ADDR(0, 0,   4),
    SFF8636_CDR_LOL                     = QSFP_ADDR(0, 0,   5),
    SFF8636_TEMP_FLAGS                  = QSFP_ADDR(0, 0,   6),
    SFF8636_VOLT_FLAGS                  = QSFP_ADDR(0, 0,   7),
    SFF8636_RX_FLAGS                    = QSFP_ADDR(0, 0,   9),
    SFF8636_TX_FLAGS                    = QSFP_ADDR(0, 0,  11),
    SFF8636_TEMPERATURE                 = QSFP_ADDR(0, 0,  22),
    SFF8636_SUPPLY_VOLTAGE              = QSFP_ADDR(0, 0,  26),
    SFF8636_RX_POWER                    = QSFP_ADDR(0, 0,  34),
    SFF8636_RX_POWER_LANE2              = QSFP_ADDR(0, 0,  36),
    SFF8636_RX_POWER_LANE3              = QSFP_ADDR(0, 0,  38),
    SFF8636_RX_POWER_LANE4              = QSFP_ADDR(0, 0,  40),
    SFF8636_TX_BIAS                     = QSFP_ADDR(0, 0,  42),
    SFF8636_TX_BIAS_LANE2               = QSFP_ADDR(0, 0,  44),
    SFF8636_TX_BIAS_LANE3               = QSFP_ADDR(0, 0,  46),
    SFF8636_TX_BIAS_LANE4               = QSFP_ADDR(0, 0,  48),
    SFF8636_TX_POWER                    = QSFP_ADDR(0, 0,  50),
    SFF8636_TX_POWER_LANE2              = QSFP_ADDR(0, 0,  52),
    SFF8636_TX_POWER_LANE3              = QSFP_ADDR(0, 0,  54),
    SFF8636_TX_POWER_LANE4              = QSFP_ADDR(0, 0,  56),
    SFF8636_TX_DISABLE                  = QSFP_ADDR(0, 0,  86),
    SFF8636_POWER_ENABLE                = QSFP_ADDR(0, 0,  93),
    SFF8636_LOS_IRQ_MASK                = QSFP_ADDR(0, 0, 100),
    SFF8636_TX_FAULT_IRQ_MASK           = QSFP_ADDR(0, 0, 101),
    SFF8636_CDR_LOL_IRQ_MASK            = QSFP_ADDR(0, 0, 102),
    SFF8636_TEMPERATURE_IRQ_MASK        = QSFP_ADDR(0, 0, 103),
    SFF8636_VOLTAGE_IRQ_MASK            = QSFP_ADDR(0, 0, 104),
    SFF8636_VENDOR_IRQ_MASK             = QSFP_ADDR(0, 0, 105),
    SFF8636_CLS8_MAX_POWER              = QSFP_ADDR(0, 0, 107),
    SFF8636_FREE_SIDE_PROP              = QSFP_ADDR(0, 0, 110),
    SFF8636_CHANNEL_INFO                = QSFP_ADDR(0, 0, 113),
    SFF8636_ID                          = QSFP_ADDR(0, 0, 128),
    SFF8636_DDM_TH                      = QSFP_ADDR(0, 3, 128),
    SFF8636_RX_POWER_IRQ_MASK           = QSFP_ADDR(0, 3, 242),
    SFF8636_TX_BIAS_IRQ_MASK            = QSFP_ADDR(0, 3, 244),
    SFF8636_TX_POWER_IRQ_MASK           = QSFP_ADDR(0, 3, 246),
    SFF8636_RESERVED_IRQ_MASK           = QSFP_ADDR(0, 3, 248),
    SFF8636_PARAM_CFG                   = QSFP_ADDR(0, 0x20, 200),
    SFF8636_PARAM_THRESHOLD             = QSFP_ADDR(0, 0x21, 128),
};

enum {
    /* rev_spec
     * SFF8636_REV_UNSPEC can be used up to SFF-8636 rev 2.5 exclusive.
     */
    SFF8636_REV_UNSPEC          = 0,
    SFF8636_REV_8436_4_8        = 1,
    SFF8636_REV_8436_4_8P       = 2,
    SFF8636_REV_8636_1_3        = 3,
    SFF8636_REV_8636_1_4        = 4,
    SFF8636_REV_8636_1_5        = 5,
    SFF8636_REV_8636_2_0        = 6,
    SFF8636_REV_8636_2_5        = 7,
    SFF8636_REV_8636_2_8        = 8,

};

/* Page 0 byte 128 */
struct sff8636_eeprom_base {
    /* byte 128 */
    u8 phys_id;
    u8 phys_ext_id;
    u8 connector;

    /* byte 131 */
    u8 e40g_active:1;
    u8 e40g_base_lr4:1;
    u8 e40g_base_sr4:1;
    u8 e40g_base_cr4:1;
    u8 e10g_base_sr:1;
    u8 e10g_base_lr:1;
    u8 e10g_base_lrm:1;
    u8 ecom_extended:1; /* byte 192 has details */

    /* byte 132 */
    u8 sonet_oc48_short_reach:1;
    u8 sonet_oc48_smf_intermediate_reach:1;
    u8 sonet_oc48_smf_long_reach:1;
    u8 reserved_2:5;

    /* byte 133 */
    u8 reserved_3:4;
    u8 sas_3gbps:1;
    u8 sas_6gbps:1;
    u8 sas_12gbps:1;
    u8 sas_24gbps:1;

    u8 e1000_base_sx:1;
    u8 e1000_base_lx:1;
    u8 e1000_base_cx:1;
    u8 e1000_base_t:1;
    u8 reserved_4:4;

    /* byte 135 */
    u8 fc_tech_electrical_inter_enclosure:1;
    u8 fc_tech_lc:1;
    u8 reserved_5:1;
    u8 fc_ll_m:1;
    u8 fc_ll_l:1;
    u8 fc_ll_i:1;
    u8 fc_ll_s:1;
    u8 fc_ll_v:1;

    u8 reserved_6:4;
    u8 longwave_laser:1;
    u8 longwave_laser_w_ofc:1;
    u8 longwave_laser_wo_ofc:1;
    u8 electrical_intra_enclosure:1;

    /* byte 137 */
    u8 fc_media_sm:1;
    u8 fc_media_om3:1;
    u8 fc_media_m5:1;
    u8 fc_media_m6:1;
    u8 fc_media_tv:1;
    u8 fc_media_mi:1;
    u8 fc_media_tp:1;
    u8 fc_media_tw:1;

    u8 fc_speed_100:1;
    u8 fc_extended:1;  /* check byte 192 */
    u8 fc_speed_200:1;
    u8 fc_speed_3200:1;
    u8 fc_speed_400:1;
    u8 fc_speed_1600:1;
    u8 fc_speed_800:1;
    u8 fc_speed_1200:1;

    /* byte 139 */
    u8 encoding;
    u8 br_nominal;
    u8 ext_ratesel_spec;

    /* byte 142 */
    u8 length[5];

    /* byte 147 */
    u8 device_tech;

    /* byte 148 */
    char vendor_name[16];

    /* byte 164 */
    u8 ext_module;
    char vendor_oui[3];

    /* byte 168 */
    char vendor_pn[16];

    /* byte 184 */
    char vendor_rev[2];
    union {
        __be16 wavelength;
        u8 copper_atten[2];
    } __packed;
    __be16 wavelength_tolerance;

    /* byte 190 */
    u8 max_case_temp;
    /* byte 191 */
    u8 cc_base;
} __packed;

/* Page 0 byte 192 */
struct sff8636_eeprom_ext {
    /* byte 192 */
    u8 link_codes;

    u8 rx_amp_prg:1;
    u8 rx_emp_prg:1;
    u8 tx_eq_prg:1;
    u8 tx_eq_auto_adap:1;
    u8 tx_adap_eq_freeze:1;
    u8 intl_gpio:1;
    u8 lpmode_gpio:1;
    u8 reserved:1;

    /* byte 194 */
    u8 tx_squelch_impl:1;
    u8 tx_squelch_dis_impl:1;
    u8 rx_output_dis_impl:1;
    u8 rx_squelch_dis_impl:1;
    u8 rx_cdr_lol_impl:1;
    u8 tx_cdr_lol_impl:1;
    u8 rx_cdr_ctrl_impl:1;
    u8 tx_cdr_ctrl_impl:1;

    u8 page20_21:1;
    u8 tx_los_impl:1;
    u8 tx_squelch_oma_impl:1;
    u8 tx_fault_impl:1;
    u8 tx_dis_impl:1;
    u8 rate_select_impl:1;
    u8 page1:1;
    u8 page2:1;

    /* byte 196 */
    char vendor_sn[16];

    /* byte 212 */
    u8 datecode[8];

    /* byte 220 */
    union {
        u8 diagmon;
        struct {
            u8 reserved1:2;
            u8 tx_mon_impl:1;
            u8 rx_mon_impl:1;
            u8 volt_mon_impl:1;
            u8 temp_mon_impl:1;
            u8 reserved2:2;
        };
    };

    /* byte 221 */
    u8 enh_options;
    u8 baud_rate_nominal;

    /* byte 223 */
    u8 cc_ext;
} __packed;

struct sff8636_eeprom_id {
    struct sff8636_eeprom_base base;
    struct sff8636_eeprom_ext  ext;
} __packed;

struct sff8636_id_stat {
    u8 phys_id;
    u8 rev_spec;

    u8 data_not_ready:1;
    u8 irq_deasserted:1;
    u8 flat_mem:1;
    u8 reserved:5;
} __packed;

struct sff8636_los {
    /* byte 3 */
    u8 rx_los:4;
    u8 tx_los:4;
} __packed;

struct sff8636_tx_fault {
    /* byte 4 */
    u8 tx_fault:4;
    u8 tx_adap_eq_in_fail:4;
} __packed;

struct sff8636_cdr_lol {
    /* byte 5 */
    u8 rx_cdr_lol:4;
    u8 tx_cdr_lol:4;
} __packed;

struct sff8636_temp_flags {
    /* byte 6 */
    u8 init_complete:1;
    u8 tc_ready:1;
    u8 reserved_1:2;
    u8 temp_low_warn:1;
    u8 temp_high_warn:1;
    u8 temp_low_alarm:1;
    u8 temp_high_alarm:1;
} __packed;

struct sff8636_volt_flags {
    /* byte 7 */
    u8 reserved_2:4;
    u8 volt_low_warn:1;
    u8 volt_high_warn:1;
    u8 volt_low_alarm:1;
    u8 volt_high_alarm:1;
} __packed;

struct sff8636_rx_flags {
    /* byte 9 */
    u8 rx2_power_low_warn:1;
    u8 rx2_power_high_warn:1;
    u8 rx2_power_low_alarm:1;
    u8 rx2_power_high_alarm:1;
    u8 rx1_power_low_warn:1;
    u8 rx1_power_high_warn:1;
    u8 rx1_power_low_alarm:1;
    u8 rx1_power_high_alarm:1;

    /* byte 10 */
    u8 rx4_power_low_warn:1;
    u8 rx4_power_high_warn:1;
    u8 rx4_power_low_alarm:1;
    u8 rx4_power_high_alarm:1;
    u8 rx3_power_low_warn:1;
    u8 rx3_power_high_warn:1;
    u8 rx3_power_low_alarm:1;
    u8 rx3_power_high_alarm:1;
} __packed;

struct sff8636_tx_flags {
    /* byte 11 */
    u8 tx2_bias_low_warn:1;
    u8 tx2_bias_high_warn:1;
    u8 tx2_bias_low_alarm:1;
    u8 tx2_bias_high_alarm:1;
    u8 tx1_bias_low_warn:1;
    u8 tx1_bias_high_warn:1;
    u8 tx1_bias_low_alarm:1;
    u8 tx1_bias_high_alarm:1;

    /* byte 12 */
    u8 tx4_bias_low_warn:1;
    u8 tx4_bias_high_warn:1;
    u8 tx4_bias_low_alarm:1;
    u8 tx4_bias_high_alarm:1;
    u8 tx3_bias_low_warn:1;
    u8 tx3_bias_high_warn:1;
    u8 tx3_bias_low_alarm:1;
    u8 tx3_bias_high_alarm:1;

    /* byte 13 */
    u8 tx2_power_low_warn:1;
    u8 tx2_power_high_warn:1;
    u8 tx2_power_low_alarm:1;
    u8 tx2_power_high_alarm:1;
    u8 tx1_power_low_warn:1;
    u8 tx1_power_high_warn:1;
    u8 tx1_power_low_alarm:1;
    u8 tx1_power_high_alarm:1;

    /* byte 14 */
    u8 tx4_power_low_warn:1;
    u8 tx4_power_high_warn:1;
    u8 tx4_power_low_alarm:1;
    u8 tx4_power_high_alarm:1;
    u8 tx3_power_low_warn:1;
    u8 tx3_power_high_warn:1;
    u8 tx3_power_low_alarm:1;
    u8 tx3_power_high_alarm:1;
} __packed;

struct sff8636_ddm_thresholds {
    u16 temp_high_alarm;
    __be16 temp_low_alarm;
    u16 temp_high_warn;
    __be16 temp_low_warn;
    u8  res_1[8];
    u16 volt_high_alarm;
    u16 volt_low_alarm;
    u16 volt_high_warn;
    u16 volt_low_warn;
    u8  res_2[8];
    u8  vendor_specific[16];
    u16 rxpwr_high_alarm;
    u16 rxpwr_low_alarm;
    u16 rxpwr_high_warn;
    u16 rxpwr_low_warn;
    u16 bias_high_alarm;
    u16 bias_low_alarm;
    u16 bias_high_warn;
    u16 bias_low_warn;
    u16 txpwr_high_alarm;
    u16 txpwr_low_alarm;
    u16 txpwr_high_warn;
    u16 txpwr_low_warn;
}__packed;

struct sff8636_param_thresholds {
    __be16 param_high_alarm;
    __be16 param_low_alarm;
    __be16 param_high_warn;
    __be16 param_low_warn;
}__packed;

struct sff8636_param_cfg {
    /* MSB */
    u8 channel_num:2;
    u8 param_mon_type:1;
    u8 reserved:1;
    u8 threshold_id:4;

    /* LSB */
    u8 param_type;
}__packed;

#endif
